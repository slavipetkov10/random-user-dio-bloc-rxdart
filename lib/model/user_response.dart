import 'user.dart';

class UserResponse {
  final List<User> results;
  final String error;

  UserResponse(this.results, this.error);

  UserResponse.fromJson(Map<String, dynamic> json)
      : results =
            (json['results'] as List).map((i) => new
            User.fromJson(i)).toList(),
        error = "";

  UserResponse.withError(String errorValue)
      : results = List(),
        error = errorValue;
}

//
// import 'package:random_user_dio_bloc_rxdart/model/user.dart';
//
// class UserResponse {
//   final List<User> results;
//   final String error;
//
//   UserResponse(this.results, this.error);
//
//   // assign values to the variables results,errors
//   UserResponse.fromJson(Map<String, dynamic> json) :
//     results = (json["results"] as List)
//         .map((i) => new User.fromJson(i))
//         .toList(),
//     error = "";
//
//   UserResponse.withError(String errorValue)
//       : results = List(),
//         error = errorValue;
// }
