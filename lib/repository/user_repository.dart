import 'package:random_user_dio_bloc_rxdart/model/user_response.dart';
import 'package:random_user_dio_bloc_rxdart/repository/user_api_provider.dart';

class UserRepository{
  UserApiProvider _apiProvider = UserApiProvider();
  Future<UserResponse> getUser(){
    return _apiProvider.getUser();
  }
}



//
//
// import 'package:random_user_dio_bloc_rxdart/model/user_response.dart';
// import 'package:random_user_dio_bloc_rxdart/repository/user_api_provider.dart';
//
// class UserRepository{
//   UserApiProvider _apiProvider = UserApiProvider();
//
//   Future<UserResponse> getUser(){
//     return _apiProvider.getUser();
//   }
// }